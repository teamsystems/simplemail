# SimpleMail
Used to create basic emails. Use is discouraged and is only maintained for backwards
compatibility with older TEAM Framework based application that require this package.

## Install with Composer
To install with [Composer](https://getcomposer.org/), simply require the latest version of this package.
```
composer require team/simplemail
```