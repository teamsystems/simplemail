<?php
/**
 * SimpleMail Class
 *
 * Used for simple creation of text, html, and MIME type email messages
 *
 * @package		simplemail
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class SimpleMail
{
    const PRIORITY_HIGHEST = 1;
	const PRIORITY_HIGH = 2;
	const PRIORITY_NORMAL = 3;
	const PRIORITY_LOW = 4;
	const PRIORITY_LOWEST = 5;

	protected $_aTo;
	protected $_aCc;
	protected $_aBcc;
	protected $_sFrom;
	protected $_sSender;
	protected $_sSubject;
	protected $_sReplyTo;
	protected $_sReturnPath;
	protected $_sCharset;
	protected $_sEncoding;
	protected $_bReceipt;
	protected $_iPriority;
	protected $_aAttachments;
	protected $_sText;
	protected $_sHtml;
	protected $_sAdditionalParams;

	protected $_aHeaders;
	protected $_aRawHeaders;
	protected $_sBody;

    protected $_XMailer;

	public function __construct ()
	{
		$this->_sCharset = 'us-ascii';
		$this->_sEncoding = '7bit';
		$this->_bReceipt = FALSE;

		$this->_aTo = array ();
		$this->_aCc = array ();
		$this->_aBcc = array ();
		$this->_aAttachments = array ();
		$this->_aRawHeaders = array ();

        $this->_XMailer = 'TEAM Framework/SimpleMail';
	}

	/**
	 * Set recipient
	 *
	 * @param string|array array or string of addresses
	 */
	public function setTo ($mAddr)
	{
		$this->_aTo = $this->_buildAddrList ($mAddr);
	}

	/**
	 * Set the Cc
	 *
	 * @param string|array array or string of addresses
	 */
	public function setCc ($mAddr)
	{
		$this->_aCc = $this->_buildAddrList ($mAddr);
	}

	/**
	 * Set the Bcc
	 *
	 * @param string|array array or string of addresses
	 */
	public function setBcc ($mAddr)
	{
		$this->_aBcc = $this->_buildAddrList ($mAddr);
	}

	/**
	 * Set the from address
	 *
	 * @param string email of sender
	 */
	public function setFrom ($sAddr)
	{
		$this->_sFrom = $sAddr;
	}

	/**
	 * Set the sender address
	 *
	 * @param string email of sender
	 */
	public function setSender ($sAddr)
	{
		$this->_sSender = $sAddr;
	}

	/**
	 * Set the reply-to
	 *
	 * @param string email to reply to
	 */
	public function replyTo ($sAddr)
	{
		$this->_sReplyTo = $sAddr;
	}

	/**
	 * Set the return path
	 *
	 * @param string return address
	 */
	public function returnPath ($sAddr)
	{
		$this->_sReturnPath = $sAddr;
	}

	/**
	 * Set the message priority
	 *
	 * @param int priority
	 */
	public function setPriority ($iPriority)
	{
		if ( ! is_int ($iPriority) or $iPriority < 1 or $iPriority > 5 )
		{
			$iPriority = self::PRIORITY_NORMAL;
		}

		$this->_iPriority = $iPriority;
	}

	/**
	 * Set charset
	 *
	 * @param string $sCharset
	 */
	public function setCharset ($sCharset)
	{
		$this->_sCharset = $sCharset;
	}

	/**
	 * Set encoding
	 *
	 * @param string $sEncoding
	 */
	public function setEncoding ($sEncoding)
	{
		$this->_sEncoding = $sEncoding;
	}

	/**
	 * Set the message subject
	 *
	 * @param string subject
	 */
	public function setSubject ($sSubject)
	{
		$this->_sSubject = $sSubject;
	}

	/**
	 * Request receipt of message delivery
	 *
	 * @param bool $bFlag
	 */
	public function requestReceipt ($bFlag = TRUE)
	{
		$this->bReceipt = $bFlag;
	}

	/**
	 * Set the html body of the email
	 *
	 * @param string html code
	 * @param bool $bConvertToText
	 */
	public function setHtml ($sHtml, $bConvertToText = FALSE)
	{
		$this->_sHtml = $sHtml;

		if ( $bConvertToText === TRUE )
		{
			$this->setText ($this->_convertHTMLtoText ($sHtml));
		}
	}

	/**
	 * Set the tex tof the email message
	 *
	 * @param string text
	 */
	public function setText ($sText)
	{
		$this->_sText = $sText;
	}

	/**
	 * Add a attachement
	 *
	 * @param string $sData
	 * @param string $sFilename
	 * @param string $sMimeType
	 * @param string $sDisposition
	 */
	public function addAttachment ($sData, $sFilename, $sMimeType = 'application/x-unknown-content-type', $sDisposition = 'attachment')
	{
		$this->_aAttachments[] = array (
			'data' => $sData,
			'name' => $sFilename,
			'mime' => $sMimeType,
			'disp' => $sDisposition
		);
	}

	/**
	 * Set a raw header
     *
     * @param string $sHeader
     * @since 7.09
	 */
	public function setRawHeader ($sHeader)
	{
		$this->_aRawHeaders[] = $sHeader;
	}

    /**
     * Set additional params to pass to mail
     *
     * @param string $sParams
     * @since 7.10
     */
    public function setAdditionalParams ($sParams)
    {
        $this->_sAdditionalParams = $sParams;
    }

    /**
     * Set the X-Mailer header
     *
     * @param string $sXMailer
     * @since 7.10
     */
    public function setXMailer ($sXMailer)
    {
        $this->_XMailer = $sXMailer;
    }

	protected function _convertHTMLtoText ($sHtml)
	{
		$sText = str_replace (array ('<p>', '</p>', '<br />'), array('', "\n\n", "\n"), $sHtml);

		return strip_tags ($sText);
	}

	/**
	 * Build a array of email addresses
	 *
	 * @param unknown_type $mAddr
	 */
	protected function _buildAddrList ($mAddr)
	{
		$aReturn = array ();

		if ( is_array ($mAddr) )
		{
			$aReturn = $mAddr;
		}
		else
		{
			$aReturn[] = $mAddr;
		}

		return $aReturn;
	}

	/**
	 * Build the message
	 */
	protected function _buildMessage ()
	{
		$aHeaders = array ();
		$sBody = '';

		if ( count ($this->_aCc) > 0 )
		{
			$aHeaders[] = 'Cc: ' . implode (', ', $this->_aCc);
		}
		if ( count ($this->_aBcc) > 0 )
		{
			$aHeaders[] = 'Bcc: ' . implode (', ', $this->_aBcc);
		}
		if ( $this->_sFrom )
		{
			$aHeaders[] = 'From: ' . $this->_sFrom;
		}
		if ( $this->_sReplyTo )
		{
			$aHeaders[] = 'Reply-To: ' . $this->_sReplyTo;
		}

		if ( $this->_bReceipt === TRUE and $this->_sReplyTo )
		{
			$aHeaders[] = 'Disposition-Notification-To: ' . $this->_sReplyTo;
		}
		elseif ( $this->_bReceipt === TRUE and $this->_sFrom )
		{
			$aHeaders[] = 'Disposition-Notification-To: ' . $this->_sFrom;
		}

		if ( $this->_iPriority )
		{
			$aPriorities = array(
				1 => '1 (Highest)',
				2 => '2 (High)',
				3 => '3 (Normal)',
				4 => '4 (Low)',
				5 => '5 (Lowest)'
			);

			$aHeaders[] = 'X-Priority: ' . $aPriorities[$this->_iPriority];
		}

		if ( $this->_sReturnPath )
		{
			$aHeaders[] = 'Return-Path: ' . $this->_sReturnPath;
		}

		if ( $this->_sSender )
		{
			$aHeaders[] = 'Sender: ' . $this->_sSender;
		}

		// Add raw headers
		$aHeaders = array_merge ($aHeaders, $this->_aRawHeaders);


        if ( $this->_sText and $this->_sHtml and count ($this->_aAttachments) > 0 )
		{
            // Message needs to be multipart with multiple bounds

			$sBound = randomString (32);
			$sNextBound = randomString (32);

			$sBody = 'This is a multi-part message in MIME format.' . "\n\n";
            $sBody .= '--' . $sBound . "\n" . 'Content-Type: multipart/alternative;' . "\n" . ' boundary="' . $sNextBound . '"' . "\n\n";

			$sBody .= '--' . $sNextBound . "\n" . 'Content-Type: text/plain;' . "\n" . ' charset="' . $this->_sCharset . '"' . "\n"
				. 'Content-Transfer-Encoding: ' . $this->_sEncoding . "\n\n" . $this->_sText . "\n\n";
			$sBody .= '--' . $sNextBound . "\n" . 'Content-Type: text/html;' . "\n" . ' charset="' . $this->_sCharset . '"' . "\n"
				. 'Content-Transfer-Encoding: ' . $this->_sEncoding . "\n\n" . $this->_sHtml . "\n\n";

            $sBody .= "\n" . '--' .$sNextBound . '--' . "\n\n";

			$aHeaders[] = 'Content-Transfer-Encoding: ' . $this->_sEncoding;
            $aHeaders[] = 'Content-Type: multipart/mixed;' . "\n" . ' boundary="' . $sBound . '"';

            foreach ($this->_aAttachments as $aFile)
            {
                $sBody .= "\n" . '--' . $sBound . "\n" . 'Content-Type: ' . $aFile['mime'] . ';' . "\n" . ' name="' . $aFile['name'] . '"'
                . "\n" . 'Content-Transfer-Encoding: base64' . "\n" . 'Content-Disposition: ' . $aFile['disp'] . ';' . "\n"
                . ' filename="' . $aFile['name'] . '"' . "\n\n"
                . chunk_split (base64_encode ($aFile['data']));
            }

			$sBody .= "\n" . '--' .$sBound . '--';
		}
		elseif ( ( ($this->_sText and $this->_sHtml) or count ($this->_aAttachments) > 0 ) )
		{
            // Message is either multipart or alternative

			$sBound = randomString (32);

			$sBody = 'This is a multi-part message in MIME format.' . "\n\n";

			if ( $this->_sText )
			{
				$sBody .= '--' . $sBound . "\n" . 'Content-Type: text/plain;' . "\n" . ' charset="' . $this->_sCharset . '"' . "\n"
				. 'Content-Transfer-Encoding: ' . $this->_sEncoding . "\n\n" . $this->_sText . "\n\n";
			}

			if ( $this->_sHtml )
			{
				$sBody .= '--' . $sBound . "\n" . 'Content-Type: text/html;' . "\n" . ' charset="' . $this->_sCharset . '"' . "\n"
				. 'Content-Transfer-Encoding: ' . $this->_sEncoding . "\n\n" . $this->_sHtml . "\n\n";
			}

			$aHeaders[] = 'Content-Transfer-Encoding: ' . $this->_sEncoding;

			if ( count ($this->_aAttachments) > 0 )
			{
				$aHeaders[] = 'Content-Type: multipart/mixed;' . "\n" . ' boundary="' . $sBound . '"';

				foreach ($this->_aAttachments as $aFile)
				{
					$sBody .= "\n" . '--' . $sBound . "\n" . 'Content-Type: ' . $aFile['mime'] . ';' . "\n" . ' name="' . $aFile['name'] . '"'
					. "\n" . 'Content-Transfer-Encoding: base64' . "\n" . 'Content-Disposition: ' . $aFile['disp'] . ';' . "\n"
					. ' filename="' . $aFile['name'] . '"' . "\n\n"
					. chunk_split (base64_encode ($aFile['data']));
				}
			}
			else
			{
				$aHeaders[] = 'Content-Type: multipart/alternative;' . "\n" . ' boundary="' . $sBound . '"';
			}

			$sBody .= "\n" . '--' .$sBound . '--';
		}
		else
		{
            // Message is a simple text or html email

			if ( $this->_sHtml )
			{
				$sBody = $this->_sHtml;
				$sContentType = 'text/html';
			}
			else
			{
				$sBody = $this->_sText;
				$sContentType = 'text/plain';
			}

			$aHeaders[] = 'Content-Type: ' . $sContentType . ';' . "\n" . ' charset="' . $this->_sCharset . '"';
			$aHeaders[] = 'Content-Transfer-Encoding: ' . $this->_sEncoding;
		}

        if ( $this->_XMailer )
        {
         $aHeaders[] = 'X-Mailer: ' . $this->_XMailer;
        }

        $aHeaders[] = 'MIME-Version: 1.0';

		$this->_aHeaders = $aHeaders;
		$this->_sBody = preg_replace ("/\r?\n/", PHP_EOL, $sBody);
	}

	/**
	 * Send the email
     *
     * @param string|array $mTo Overwrite to address list
	 */
	public function send ($mTo = NULL)
	{
		$this->_buildMessage ();

        $aTo = ( $mTo !== NULL )
            ? $this->_buildAddrList ($mTo)
            : $this->_aTo;


		return mail (implode (', ', $aTo), $this->_sSubject, $this->_sBody, implode (PHP_EOL, $this->_aHeaders), $this->_sAdditionalParams);
	}
}